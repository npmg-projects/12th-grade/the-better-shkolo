/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package school.students;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import school.DbService;
import school.students.Student;
import school.students.StudentsInterface;

/**
 *
 * @author a1
 */
public class StudentsService implements StudentsInterface {

    private final DbService dbService;
    private final StudentsMapper mapper;

    public StudentsService() {
        dbService = new DbService();
        mapper = new StudentsMapper();
    }

    @Override
    public void createStudent(Student student) {
        String query = "INSERT INTO students (first_name, last_name, UCN, class_number, valid) VALUES('"
                + student.getFirstName() + "', '" + student.getLastName() + "', '" + student.getUCN() + "', '"
                + student.getClassNumber() + "', '" + student.getValid() + "');";
        dbService.doInsert(query);
    }

    @Override
    public List<Student> getStudents() {
        String query =  "SELECT s.first_name, s.id as student_id, s.last_name, s.UCN, s.class_number, s.class_id, s.valid, c.id AS classID, c.grade, c.class_name "
            + "FROM students s "
            + "INNER JOIN classes c ON s.class_id = c.id "
            + "ORDER BY s.first_name ASC, s.last_name ASC;";
        ResultSet studentsSet = dbService.executeQuery(query);
        try {
            List<Student> students = mapper.processResultSet(studentsSet);
            return students;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Student getStudent(Integer id) {
        String query = "SELECT s.first_name, s.id as student_id, s.last_name, s.UCN, s.class_number, s.class_id, s.valid, c.id AS classID, c.grade, c.class_name "
            + "FROM students s "
            + "INNER JOIN classes c ON s.class_id = c.id "
            + "WHERE s.id = " + id + ";";
        ResultSet studentSet = dbService.executeQuery(query);
        try {
            Student student = mapper.mapRow(studentSet);
            return student;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Student updateStudent(Student student) {
        String query = "UPDATE students\n"
                + "SET first_name = '" + student.getFirstName() + "',\n"
                + "last_name = '" + student.getLastName() + "',\n"
                + "UCN = '" + student.getUCN() + "',\n"
                + "class_id = " + student.getSchoolClass().getId() + "',\n"
                + "class_number = '" + student.getClassNumber() + "',\n"
                + "WHERE id = " + student.getId() + ";";
        dbService.doInsert(query);

        return getStudent(student.getId());
    }

    @Override
    public Student softDeleteStudent(Integer id) {
        String query = "UPDATE students\n"
                + "SET valid = FALSE,\n"
                + "WHERE id = " + id + ";";
        ResultSet updatedStudent = dbService.executeQuery(query);
        try {
            Student student = mapper.mapRow(updatedStudent);
            return student;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}