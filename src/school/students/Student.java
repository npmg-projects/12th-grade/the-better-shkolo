/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package school.students;

import school.classes.SchoolClass;

/**
 *
 * @author a1
 */
public class Student {

    private Integer id;
    private String firstName;
    private String lastName;
    private String UCN;
    private Integer classNumber;
    private SchoolClass schoolClass;
    private Boolean valid;

    // Constructor with all properties
    public Student(Integer id, String firstName, String lastName, String UCN, Integer classNumber, SchoolClass schoolClass, Boolean valid) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.UCN = UCN;
        this.classNumber = classNumber;
        this.schoolClass = schoolClass;
        this.valid = valid;
    }
    
    public Student(Integer id, String firstName, String lastName, String UCN, Integer classNumber, Boolean valid) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.UCN = UCN;
        this.classNumber = classNumber;
        this.valid = valid;
    }

    // Constructor without id
    public Student(String firstName, String lastName, String UCN, Integer classNumber, SchoolClass schoolClass, Boolean valid) {
        this.id = null;
        this.firstName = firstName;
        this.lastName = lastName;
        this.UCN = UCN;
        this.classNumber = classNumber;
        this.schoolClass = schoolClass;
        this.valid = valid;
    }

    //Getter for id
    public Integer getId() {
        return id;
    }

    //Setter for id
    public void setId(Integer id) {
        this.id = id;
    }

    //Getter for firstName
    public String getFirstName() {
        return firstName;
    }

    //Setter for firstName
    public void
            setFirstName(String firstName) {
        this.firstName = firstName;
    }

    //Getter for lastName
    public String getLastName() {
        return lastName;
    }

    //Setter for lastName
    public void
            setLastName(String lastName) {
        this.lastName = lastName;
    }

    //Getter for UCN
    public String getUCN() {
        return UCN;
    }

    //Setter for UCN
    public void setUCN(String UCN) {
        this.UCN = UCN;
    }

    //Getter for classNumber
    public Integer getClassNumber() {
        return classNumber;
    }

    //Setter for classNumber
    public void setClassNumber(Integer classNumber) {
        this.classNumber = classNumber;
    }

    //Getter for schoolClass
    public SchoolClass getSchoolClass() {
        return schoolClass;
    }

    //Setter for schoolClass
    public void setSchoolClass(SchoolClass schoolClass) {
        this.schoolClass = schoolClass;
    }

    //Getter for valid
    public Boolean getValid() {
        return valid;
    }

    //Setter for valid
    public void
            setValid(Boolean valid) {
        this.valid = valid;
    }
}
