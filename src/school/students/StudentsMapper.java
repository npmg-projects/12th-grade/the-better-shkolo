package school.students;


import java.sql.ResultSet;
import java.sql.SQLException;
import school.RowMapper;
import school.classes.SchoolClass;
import school.students.Student;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author betinayy
 */
public class StudentsMapper extends RowMapper<Student> {

    @Override
    public Student mapRow(ResultSet rs) throws SQLException {
//        SchoolClass schoolClass = new SchoolClass(rs.getInt("class_id"), rs.getBoolean("valid"), rs.getInt("grade"), rs.getString("class_name"));
        Student student = new Student(rs.getInt("student_id"), rs.getString("first_name"), rs.getString("last_name"), rs.getString("UCN"), rs.getInt("class_number"), rs.getBoolean("valid"));
        return student;
    }
}