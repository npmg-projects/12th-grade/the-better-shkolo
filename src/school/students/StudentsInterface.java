/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package school.students;

import java.util.ArrayList;
import java.util.List;
import school.students.Student;

/**
 *
 * @author a1
 */
public interface StudentsInterface {
    public abstract void createStudent(Student teacher);
    public abstract List<Student> getStudents();
    public abstract Student getStudent(Integer id);
    public abstract Student updateStudent(Student student);//, Integer specialisationsID
    public abstract Student softDeleteStudent(Integer id);
}
