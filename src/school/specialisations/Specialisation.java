/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package school.specialisations;

/**
 *
 * @author a1
 */
public class Specialisation {
    private Integer id;
    private String type;

    public void setID(Integer id){
        this.id = id;
    }
    
    public Integer getID(){
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {//Мутатор на name
        this.type = type;
    }

    
    public Specialisation(String type) {//Конструктор за общо ползване
        setType(type);
    }
    
    public Specialisation(Integer id, String type){
        setID(id);
        setType(type);
    }
    
    public Specialisation() {
        setID(null);
        setType(null);
    }
}
