/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package school.specialisations;

import java.util.ArrayList;
import java.util.List;

import school.specialisations.Specialisation;

/**
 *
 * @author a1
 */
public interface SpecialisationsInterface {
    public abstract void createSpecialisation(Specialisation specialisation);
    public abstract List<Specialisation> getSpecialisations();
    public abstract Specialisation getSpecialisation(Integer id);
    public abstract Specialisation updateSpecialisation(Specialisation specialisation);
}
