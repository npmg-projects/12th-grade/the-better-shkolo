/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package school.specialisations;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import school.DbService;
import school.specialisations.Specialisation;
import school.specialisations.SpecialisationsInterface;

/**
 *
 * @author a1
 */
public class SpecialisationsService implements SpecialisationsInterface {
    private final DbService dbService;

    private final SpecialisationMapper specialisationMapper;
    
    public SpecialisationsService() {
        dbService = new DbService();
        this.specialisationMapper = new SpecialisationMapper();
    }

    @Override
    public void createSpecialisation(Specialisation specialisation) {
        String query = "INSERT INTO specialisations (type) VALUES('" + specialisation.getType() + " ')";
        dbService.doInsert(query);
    }

    @Override
    public List<Specialisation> getSpecialisations() {
        String query = "SELECT type, ID as ID FROM specialisations";
        ResultSet specialisationsSet = dbService.executeQuery(query);
        try {
            return specialisationMapper.processResultSet(specialisationsSet);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Specialisation getSpecialisation(Integer id) {
        String query = "SELECT type, ID as ID FROM specialisations \n" +
                "WHERE id = " + id + ";";
        ResultSet spec = dbService.executeQuery(query);
        try {
            return specialisationMapper.mapRow(spec);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Specialisation updateSpecialisation(Specialisation specialisation) {
        String query = "UPDATE specialisations SET type = '" + specialisation.getType() + "' WHERE ID =" + specialisation.getID();
        dbService.doInsert(query);
        return getSpecialisation(specialisation.getID());
    }
}
