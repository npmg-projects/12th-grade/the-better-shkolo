package school.specialisations;


import school.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SpecialisationMapper extends RowMapper<Specialisation> {
    @Override
    public Specialisation mapRow(ResultSet rs) throws SQLException {
        if (rs != null) {
            Specialisation specialisation = new Specialisation(rs.getInt("ID"), rs.getString("type"));
            return specialisation;
        }
        return null;
    }
}
