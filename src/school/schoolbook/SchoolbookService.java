package school.schoolbook;

import school.DbService;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.List;

public class SchoolbookService implements SchoolbookInterface {
    private final DbService dbService;

    private final SchoolbookMapper mapper;

    public SchoolbookService () {
        this.dbService = new DbService();
        mapper = new SchoolbookMapper();
    }

    @Override
    public void addMark(Schoolbook schoolbook) {
        Date dbDate = new Date(schoolbook.getEntered().getTime());

        String query = "INSERT INTO school_book (student_id, teacher_id, mark, entered) VALUES("
                + schoolbook.getStudent().getId() + ", " + schoolbook.getTeacher().getID() + ", "
                + schoolbook.getMark() + ", cast('" + dbDate + "' as date));";
        dbService.doInsert(query);
    }

    @Override
    public List<Schoolbook> getAllMarks() {
        String query = "SELECT " +
                "s.id AS student_id, " +
                "s.first_name AS student_firstName, " +
                "s.last_name AS student_lastName, " +
                "t.id AS teacher_id, " +
                "t.first_name AS teacher_firstName, " +
                "t.last_name AS teacher_lastName, " +
                "sb.id as schoolbook_id, " +
                "sb.mark AS mark, " +
                "sb.entered AS entered\n" +
                "FROM students AS s\n" +
                "JOIN school_book AS sb ON sb.student_id = s.id\n" +
                "JOIN teachers AS t ON sb.teacher_id = t.id;";
        ResultSet marksSet = dbService.executeQuery(query);
        try {
            List<Schoolbook> marks = mapper.processResultSet(marksSet);
            return marks;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Schoolbook> getAllMarksByStudent(Integer studentId) {
        return null;
    }

    @Override
    public Schoolbook updateSchoolbook(Schoolbook schoolbook) {
        return null;
    }

    @Override
    public void deleteMark(Integer schoolbookId) {
        String query = "DELETE FROM school_book\n" +
                "WHERE id = " + schoolbookId + ";";
        dbService.doDelete(query);
    }
}
