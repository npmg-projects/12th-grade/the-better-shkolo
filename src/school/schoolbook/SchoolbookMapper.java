package school.schoolbook;

import school.RowMapper;
import school.students.Student;
import school.teachers.Teacher;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class SchoolbookMapper extends RowMapper<Schoolbook> {
    @Override
    public Schoolbook mapRow(ResultSet rs) throws SQLException {
        Date accessDatetime = rs.getDate("entered");


        Student student = new Student(rs.getInt("student_id"), rs.getString("student_firstName"), rs.getString("student_lastName"), null, null, true);
        Teacher teacher = new Teacher(rs.getInt("teacher_id"), rs.getString("teacher_firstName"), rs.getString("teacher_lastName"), null, true);
        Schoolbook schoolbook = new Schoolbook(rs.getInt("schoolbook_id"), student, teacher, rs.getDouble("mark"), accessDatetime);
        return schoolbook;
    }
}
