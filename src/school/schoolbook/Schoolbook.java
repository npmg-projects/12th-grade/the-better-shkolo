package school.schoolbook;

import school.students.Student;
import school.teachers.Teacher;

import java.util.Date;

public class Schoolbook {
    private Integer id;
    private Student student;
    private Teacher teacher;
    private Double mark;
    private Date entered;

    public Schoolbook (Integer id, Student student, Teacher teacher, Double mark, Date entered) {
        this.id = id;
        this.student = student;
        this.teacher = teacher;
        this.mark = mark;
        this.entered = entered;
    }

    public Schoolbook (Student student, Teacher teacher, Double mark, Date entered) {
        this.student = student;
        this.teacher = teacher;
        this.mark = mark;
        this.entered = entered;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getMark() {
        return mark;
    }

    public void setMark(Double mark) {
        this.mark = mark;
    }

    public Date getEntered() {
        return entered;
    }

    public void setEntered(Date entered) {
        this.entered = entered;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
}
