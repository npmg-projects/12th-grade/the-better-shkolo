package school.schoolbook;

import java.util.List;

public interface SchoolbookInterface {
    public abstract void addMark(Schoolbook schoolbook);
    public abstract List<Schoolbook> getAllMarks();
    public abstract List<Schoolbook> getAllMarksByStudent(Integer studentId);
    public abstract Schoolbook updateSchoolbook(Schoolbook schoolbook);
    public abstract void deleteMark(Integer schoolbookId);
}
