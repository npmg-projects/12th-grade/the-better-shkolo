/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package school;


import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;


public class DbService {

    public Connection getConn(){
        Connection conn = null;
        try{
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/shkoloDb", "root", "parola1234");
            System.out.println("conn="+conn);
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return conn;
    }

    public ResultSet executeQuery(String query) {
        try {
            Connection connection = getConn();
            Statement statement = connection.createStatement();
            return statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return null;
    }

    public void doInsert(String query) {
        try {
            Connection connection = getConn();
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void doDelete(String query) {
        try {
            Connection connection = getConn();
            Statement statement = connection.createStatement();
            statement.execute(query);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
