/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package school.teachers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import school.teachers.Teacher;

/**
 *
 * @author a1
 */
public interface TeachersInterface {
    public abstract void createTeacher(Teacher teacher) throws SQLException;
    public abstract List<Teacher> getTeachers();
    public abstract Teacher getTeacherById(Integer id);
    public abstract Teacher updateTeacher(Teacher teacher);//, Integer specialisationsID
    public abstract Teacher softDeleteTeacher(Integer id);
}
