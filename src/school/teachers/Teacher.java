/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package school.teachers;

import school.specialisations.Specialisation;

/**
 *
 * @author antontd
 */
public class Teacher {

    private Integer id;
    private String firstName;
    private String lastName;
    private String UCN;
    private Specialisation specialisation;
    private Boolean valid;

    public void setID(Integer id) {
        this.id = id;
    }

    public Integer getID() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUCN() {
        return UCN;
    }

    public void setUCN(String UCN) {
        this.UCN = UCN;
    }

    public void setSpecialisation(Specialisation specialisations) {
        this.specialisation = specialisations;
    }

    public Specialisation getSpecialisation() {
        return specialisation;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public Boolean getValid() {
        return valid;
    }

    public Teacher(Integer id, String firstName, String lastName, String UCN, Boolean valid) {
        setID(id);
        setFirstName(firstName);
        setLastName(lastName);
        setUCN(UCN);
        setValid(valid);
    }

    public Teacher(String firstName, String lastName, String UCN, Specialisation specialisations, Boolean valid) {
        setFirstName(firstName);
        setLastName(lastName);
        setUCN(UCN);
        setSpecialisation(specialisations);
        setValid(valid);
    }

    public Teacher(Integer id, String firstName, String lastName, String UCN, Specialisation specialisations, Boolean valid) {
        setID(id);
        setFirstName(firstName);
        setLastName(lastName);
        setUCN(UCN);
        setSpecialisation(specialisations);
        setValid(valid);
    }

    public Teacher() {
        setID(null);
        setFirstName(null);
        setLastName(null);
        setUCN(null);
        setSpecialisation(null);
        setValid(null);
    }

    @Override
    public String toString() {
        return this.firstName + this.lastName;
    }
}
