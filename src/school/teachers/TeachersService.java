/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package school.teachers;

import school.DbService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class TeachersService implements TeachersInterface {

    private final DbService dbService;
    private final TeachersMapper teachersMapper;

    public TeachersService() {
        dbService = new DbService();
        teachersMapper = new TeachersMapper();
    }

    // create a new teacher in the db
    @Override
    public void createTeacher(Teacher teacher) {
        String query = "INSERT INTO teachers (first_name, last_name, UCN,  valid, specialisations_id) VALUES('" + teacher.getFirstName()
                + "', '" + teacher.getLastName() + "', '" + teacher.getUCN() + "', '" + (teacher.getValid() ? 1 : 0) + "', '" + teacher.getSpecialisation().getID() + " ');";
        dbService.doInsert(query);
    }

    // get all teachers from the db with their specialisation
    @Override
    public List<Teacher> getTeachers() {
        String query = "SELECT\n"
                + "  t.first_name,\n"
                + "  t.ID as teacher_id,\n"
                + "  t.last_name,\n"
                + "  t.UCN,\n"
                + "  t.valid,\n"
                + "  s.id as specialisation_id,\n"
                + "  s.type as specialisation_type\n"
                + "FROM\n"
                + "  teachers as t\n"
                + "  INNER JOIN specialisations as s ON t.specialisations_id = s.id;";
   
        ResultSet teachersSet = dbService.executeQuery(query);
        try {
            List<Teacher> teachers = teachersMapper.processResultSet(teachersSet);
            return teachers;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // get teacher by id from the db
    @Override
    public Teacher getTeacherById(Integer id) {
        String query = "SELECT\n"
                + "  t.first_name,\n"
                + "  t.ID as teacher_id,\n"
                + "  t.last_name,\n"
                + "  t.UCN,\n"
                + "  t.valid,\n"
                + "  s.id as specialisation_id,\n"
                + "  s.type as specialisation_type\n"
                + "FROM teachers\n"
                + "INNER JOIN specialisations s ON t.specialisations_id = s.id\n"
                + "WHERE id = " + id + ";";
        ResultSet teacherSet = dbService.executeQuery(query);
        try {
            Teacher teacher = teachersMapper.mapRow(teacherSet);
            return teacher;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // update teacher in the db
    @Override
    public Teacher updateTeacher(Teacher teacher) {
        String query = "UPDATE teachers\n"
                + "SET first_name = '" + teacher.getFirstName() + "',\n"
                + "    last_name = '" + teacher.getLastName() + "',\n"
                + "    specialisations_id = " + teacher.getSpecialisation().getID() + ",\n"
                + "    UCN = '" + teacher.getUCN() + "'\n"
                + "WHERE id = " + teacher.getID() + ";";
        dbService.doInsert(query);

        return getTeacherById(teacher.getID());

    }

    // mark teacher as invalid
    @Override
    public Teacher softDeleteTeacher(Integer id) {
        String query = "UPDATE teachers\n"
                + "SET valid = FALSE,\n"
                + "WHERE id = " + id + ";";
        ResultSet updatedTeacher = dbService.executeQuery(query);
        try {
            return teachersMapper.mapRow(updatedTeacher);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
