/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package school.teachers;

import java.sql.ResultSet;
import java.sql.SQLException;
import school.RowMapper;
import school.specialisations.Specialisation;

/**
 *
 * @author Betina
 */
public class TeachersMapper extends RowMapper<Teacher> {

    @Override
    public Teacher mapRow(ResultSet rs) throws SQLException {
        if (rs != null) {
            Specialisation specialisation = new Specialisation(rs.getInt("specialisation_id"), rs.getString("specialisation_type"));
            Teacher teacher = new Teacher(rs.getInt("teacher_id"), rs.getString("first_name"), rs.getString("last_name"), rs.getString("UCN"), specialisation, rs.getBoolean("valid"));
            return teacher;
        }
        return null;
    }
}
