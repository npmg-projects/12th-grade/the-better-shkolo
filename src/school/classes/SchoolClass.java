/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package school.classes;

import java.util.List;
import school.students.Student;
import school.teachers.Teacher;

/**
 *
 * @author betinayy
 */
public class SchoolClass {
    private Integer id;
    private Boolean valid;
    private Integer grade;
    private String className;
    private Teacher classTeacher;
    private List<Teacher> teachers;
    private List<Student> students;
    
    public SchoolClass(Boolean valid, Integer grade, String className) {
        this.valid = valid;
        this.grade = grade;
        this.className = className;
        this.classTeacher = classTeacher;
        this.teachers = teachers;
    }
    
    public SchoolClass(Integer id, Boolean valid, Integer grade, String className) {
        this.id = id;
        this.valid = valid;
        this.grade = grade;
        this.className = className;
    }
    
    public SchoolClass(Integer id, Boolean valid, Integer grade, String className, Teacher classTeacher, List<Teacher> teachers, List<Student> students) {
        this.id = id;
        this.valid = valid;
        this.grade = grade;
        this.className = className;
        this.classTeacher = classTeacher;
        this.teachers = teachers;
        this.students = students;
    }
    
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public Boolean getValid() {
        return this.valid;
    }
    
    public void setValid(Boolean valid) {
        this.valid = valid;
    }
    
    public Integer getGrade() {
        return this.grade;
    }
    
    public void setGrade(Integer grade) {
        this.grade = grade;
    }
    
    public String getClassName() {
        return this.className;
    }
    
    public void setClassName(String className){
        this.className = className;
    }
    
    public Teacher getClassTeacher() {
        return this.classTeacher;
    }
    
    public void setClassTeacher(Teacher teacher) {
        this.classTeacher = teacher;
    }
    
    public List<Teacher> getTeachers() {
        return this.teachers;
    }
    
    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }
    
    public List<Student> getStudents() {
        return this.students;
    }
    
    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
