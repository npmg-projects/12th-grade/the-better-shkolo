/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package school.classes;

import school.RowMapper;
import java.sql.*;
/**
 *
 * @author betinayy
 */
public class ClassesMapper extends RowMapper<SchoolClass> {
    @Override
    public SchoolClass mapRow(ResultSet rs) throws SQLException {
        SchoolClass schoolClass = new SchoolClass(rs.getInt("id"), rs.getBoolean("valid"), rs.getInt("grade"), rs.getString("class_name"));
        return schoolClass;
    }
}
