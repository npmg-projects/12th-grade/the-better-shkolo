/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package school.classes;

import school.DbService;
import school.teachers.Teacher;
import school.teachers.TeachersMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import school.students.Student;
import school.students.StudentsMapper;
import school.students.StudentsService;

/**
 *
 * @author betinayy
 */
public class SchoolClassesService implements SchoolClassesInterface {

    private final DbService dbService;
    private final ClassesMapper mapper;
    private final TeachersMapper teacherMapper;
    private final StudentsMapper studentMapper;
    private final StudentsService studentService;

    public SchoolClassesService() {
        dbService = new DbService();
        mapper = new ClassesMapper();
        teacherMapper = new TeachersMapper();
        studentMapper = new StudentsMapper();
        studentService = new StudentsService();
    }

    @Override
    public void createSchoolClass(SchoolClass schoolClass) {
        String queryClasses = "INSERT INTO classes ( valid, class_name, grade ) VALUES ('" + (schoolClass.getValid() ? 1 : 0)
                + "', '" + schoolClass.getClassName() + "', '" + schoolClass.getGrade() + " ')";

        dbService.doInsert(queryClasses);
    }

    @Override
    public SchoolClass getSchoolClassById(Integer id) {
        String query = "SELECT ID, class_name, valid, grade FROM classes WHERE ID = " + id + ";";
        ResultSet classes = dbService.executeQuery(query);
        try {
            SchoolClass schoolClass = mapper.mapRow(classes);
            schoolClass.setTeachers(getTeachers(id));
            schoolClass.setStudents(getClassStudents(id));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<SchoolClass> getSchoolClasses() {
        String query = "SELECT * FROM classes;";
        ResultSet classesSet = dbService.executeQuery(query);
        try {
            List<SchoolClass> classes = mapper.processResultSet(classesSet);
            for (SchoolClass schoolClass : classes) {
                schoolClass.setTeachers(getTeachers(schoolClass.getId()));
                schoolClass.setStudents(getClassStudents(schoolClass.getId()));
//                schoolClass.setClassTeacher(getClassTeacher(schoolClass.getId()));
            }
            return classes;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SchoolClass updateSchoolClass(SchoolClass schoolClass) {
        String query = "UPDATE classes SET class_name = " + schoolClass.getClassName()
                + ", grade = " + schoolClass.getValid() + "WHERE id = " + schoolClass.getId() + ";";
        ResultSet classes = dbService.executeQuery(query);
//        SchoolClass originalClasses = getSchoolClassById(schoolClass.getId());
//        for (Teacher teacher : schoolClass.getTeachers()) {
//            if (originalClasses.getTeachers().contains(teacher))
//        }
        try {
            return mapper.mapRow(classes);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SchoolClass softDeleteSchoolClass(Integer id) {
        String query = "UPDATE classes SET valid = 0" + "WHERE id = " + id + ";";
        ResultSet classes = dbService.executeQuery(query);
        try {
            return mapper.mapRow(classes);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setClassTeacher(SchoolClass schoolClass, Teacher teacher) {
        String query = "INSERT INTO teacher_to_class ( teacher_id, class_id, is_class_teacher) VALUES ('"
                + teacher.getID() + "', '" + schoolClass.getId() + "', '" + true + " ')";
        ResultSet classes = dbService.executeQuery(query);
        try {
            mapper.mapRow(classes);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setTeachers(Integer classId, List<Teacher> teachers) {
        for (Teacher teacher : teachers) {
            String query = "INSERT INTO teacher_to_class ( teacher_id, class_id, is_class_teacher) VALUES ('"
                    + teacher.getID() + "', '" + classId + "', '" + false + " ')";
            ResultSet classes = dbService.executeQuery(query);
            try {
                mapper.mapRow(classes);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private List<Teacher> getTeachers(Integer classId) throws SQLException {
        String query = "SELECT\n"
                + "    teachers.first_name,\n"
                + "    teachers.ID AS teacher_id,\n"
                + "    teachers.last_name,\n"
                + "    teachers.UCN,\n"
                + "    teachers.valid,\n"
                + "    teacher_to_class.is_class_teacher AS is_class_teacher,\n"
                + "    specialisations.id AS specialisation_id,\n"
                + "    specialisations.type AS specialisation_type\n"
                + "FROM ((teachers INNER JOIN teacher_to_class ON teachers.id = teacher_to_class.teacher_id) INNER JOIN specialisations ON teachers.specialisations_id = specialisations.id)"
                + "WHERE\n"
                + "  teacher_to_class.class_id = " + classId + "\n"
                + "AND teacher_to_class.is_class_teacher = 0;";
        ResultSet teachers = dbService.executeQuery(query);
        return teacherMapper.processResultSet(teachers);
    }

    private Teacher getClassTeacher(Integer classId) throws SQLException {
        String query = "SELECT\n"
                + "    teachers.first_name,\n"
                + "    teachers.ID AS teacher_id,\n"
                + "    teachers.last_name,\n"
                + "    teachers.UCN,\n"
                + "    teachers.valid,\n"
                + "    teacher_to_class.is_class_teacher AS is_class_teacher,\n"
                + "    specialisations.id AS specialisation_id,\n"
                + "    specialisations.type AS specialisation_type\n"
                + "FROM ((teachers INNER JOIN teacher_to_class ON teachers.id = teacher_to_class.teacher_id) INNER JOIN specialisations ON teachers.specialisations_id = specialisations.id)"
                + "WHERE\n"
                + "  teacher_to_class.class_id = " + classId + "\n"
                + "AND teacher_to_class.is_class_teacher = TRUE;";
        ResultSet teacher = dbService.executeQuery(query);
        return teacherMapper.mapRow(teacher);
    }

    private List<Student> getClassStudents(Integer classId) throws SQLException {
        String query = "SELECT first_name, id as student_id, last_name, UCN, class_number, valid, class_id\n"
                + "FROM students\n"
                + "WHERE class_id = " + classId + ";";

        ResultSet studentsSet = dbService.executeQuery(query);
        return studentMapper.processResultSet(studentsSet);
    }

    public void moveStudentToAnotherClass(Integer classId, List<Student> students) {
        SchoolClass schoolClass = getSchoolClassById(classId);
        for (Student student : students) {
            studentService.updateStudent(student);
        }
    }

    public void removeTeachersFromClass(Integer classId, List<Teacher> teachers) {
        String query;
        for (Teacher teacher : teachers) {
            query = "DELETE FROM teacher_to_class\n"
                + "WHERE class_id = " + classId + " AND teacher_id = " + teacher.getID() + ";";
        }
    }
}
