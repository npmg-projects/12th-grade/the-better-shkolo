/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package school.classes;

import java.util.List;

/**
 *
 * @author betinayy
 */
public interface SchoolClassesInterface {
//    public abstract SchoolClass createSchoolClass(SchoolClass schoolClass);
    public abstract void createSchoolClass(SchoolClass schoolClass);
    public abstract SchoolClass getSchoolClassById(Integer id);
    public abstract List<SchoolClass> getSchoolClasses();
    public abstract SchoolClass updateSchoolClass(SchoolClass schoolClass);
    public abstract SchoolClass softDeleteSchoolClass(Integer id);
}
