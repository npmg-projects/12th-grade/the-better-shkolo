/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package school;

import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author betinayy
 */
public abstract class RowMapper<E> {
    public List<E> processResultSet(ResultSet rs) throws SQLException {
         List<E> objectList = new ArrayList<>();

         while(rs.next()) {
             E object = mapRow(rs);
             if (object != null) objectList.add(mapRow(rs));
         }

         return objectList;
    }

    public abstract E mapRow(ResultSet rs) throws SQLException;
}
