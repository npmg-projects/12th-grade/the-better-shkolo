CREATE DATABASE shkoloDb;
USE shkoloDb; 
CREATE TABLE IF NOT EXISTS `teachers` (
	`id` int AUTO_INCREMENT NOT NULL UNIQUE,
	`valid` tinyint(1) NOT NULL,
	`first_name` varchar(30) NOT NULL,
	`last_name` varchar(30) NOT NULL,
	`UCN` varchar(10) NOT NULL,
	`specialisations_id` int NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `specialisations` (
	`id` int AUTO_INCREMENT NOT NULL UNIQUE,
	`type` varchar(50) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `classes` (
	`id` int AUTO_INCREMENT NOT NULL UNIQUE,
	`class_name` varchar(10) NOT NULL,
	`grade` int NOT NULL,
	`valid` tinyint(1) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `students` (
	`id` int AUTO_INCREMENT NOT NULL UNIQUE,
	`first_name` varchar(30) NOT NULL,
	`last_name` varchar(30) NOT NULL,
	`UCN` varchar(10) NOT NULL,
	`class_number` int NOT NULL,
	`class_id` int NOT NULL,
	`valid` tinyint(1) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `teacher_to_class` (
	`id` int AUTO_INCREMENT NOT NULL UNIQUE,
	`teacher_id` int NOT NULL,
	`class_id` int NOT NULL,
	`is_class_teacher` tinyint(1) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `school_book` (
	`id` int AUTO_INCREMENT NOT NULL UNIQUE,
	`student_id` int NOT NULL,
	`teacher_id` int NOT NULL,
	`mark` int NOT NULL,
	`entered` timestamp NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `teachers` ADD CONSTRAINT `teachers_fk5` FOREIGN KEY (`specialisations_id`) REFERENCES `specialisations`(`id`);


ALTER TABLE `students` ADD CONSTRAINT `students_fk5` FOREIGN KEY (`class_id`) REFERENCES `classes`(`id`);
ALTER TABLE `teacher_to_class` ADD CONSTRAINT `teacher_to_class_fk1` FOREIGN KEY (`teacher_id`) REFERENCES `teachers`(`id`);

ALTER TABLE `teacher_to_class` ADD CONSTRAINT `teacher_to_class_fk2` FOREIGN KEY (`class_id`) REFERENCES `classes`(`id`);
ALTER TABLE `school_book` ADD CONSTRAINT `school_book_fk1` FOREIGN KEY (`student_id`) REFERENCES `students`(`id`);

ALTER TABLE `school_book` ADD CONSTRAINT `school_book_fk2` FOREIGN KEY (`teacher_id`) REFERENCES `teachers`(`id`);